**# Project Euler solutions**

Project Euler (pronounced Oiler) is a series of challenging mathematical/computer programming problem
meant to delve into unfamiliar areas and learn new concepts in a fun and recreational manner.

1. Multiples of 3 and 5
2. Even Fibonacci Numbers
3. Largest prime factor
4. Largest palindrome product
5. Smallest multiple
6. Sum square difference
7. 10001st prime
8. Largest product in a series
9. Special Pythagorean triplet
10. Summation of primes
.
.
.


